/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, } from '@angular/core';
import { DeliveryService } from '../delivery.service';
import { Router } from '@angular/router';
import { SearchmodalComponent } from '../searchmodal/searchmodal.component';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  title = 'Choose the restaurant  you like.';
  buttonToggle: boolean;
  comingFrom: any;
  artistList: any;
  slideArtistData: ({ image: string; name: string; imgSmall: string; id: number; show?: undefined; } | { image: string; name: string; show: boolean; id: number; imgSmall?: undefined; })[];
  constructor(
    public serviceProvider: DeliveryService,
    public route: Router,
  ) {
  }

  ionViewWillEnter() {
    this.artistList = this.serviceProvider.artistData;
    this.slideArtistData = this.serviceProvider.slideselectArtists;
  }

  async searchInputClicked() {
    const modal = await this.serviceProvider.opneModal(SearchmodalComponent);
    modal.present();
  }
  getToRestroProduct(items: any) {
    this.route.navigate(['restroproducts', items]);

  }
}
