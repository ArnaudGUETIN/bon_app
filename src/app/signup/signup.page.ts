/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  signUP = [
    { name: 'Username:', placeholder: 'Enter a user name', type: 'text' },
    { name: 'Password:', placeholder: 'Create a password', type: 'password' },
    { name: 'Email:', placeholder: 'Enter a user name', type: 'email' },
    { name: 'Contact:', placeholder: 'Enter your contact', type: 'tel' },
    { name: 'Address:', placeholder: 'Enter your Address', type: 'text' },
  ];
  loginPic = '../../assets/login.jpg';
  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }
  goToHome() {
    this.router.navigate(['tabs']);
  }
  goToLogin() {
    this.router.navigate(['login']);

  }
}
