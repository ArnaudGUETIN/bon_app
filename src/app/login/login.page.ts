/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DeliveryService } from '../delivery.service';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginPic = '../../assets/login.jpg';
  constructor(
    public router: Router,
    public serviceProvider: DeliveryService
  ) { }

  ngOnInit() {
  }
  goToSignUp() {
    this.router.navigate(['signup']);
  }
  goToHome() {
    this.router.navigate(['tabs']);
  }
  async goToForgotpassword() {

    const forgotModal = await this.serviceProvider.opneModal(ForgotPasswordComponent);
    await forgotModal.present();
  }
}
