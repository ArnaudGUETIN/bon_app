/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';
import { AddaddressComponent } from '../addaddress/addaddress.component';

@Component({
  selector: 'app-address-list',
  templateUrl: './address-list.component.html',
  styleUrls: ['./address-list.component.scss'],
})
export class AddressListComponent implements OnInit {
  addList: { name: string; add: string; time: string; }[];

  constructor(
    public serviceProvider: DeliveryService
  ) {
    this.addList = this.serviceProvider.addressList;
    console.log(this.addList);
  }

  ngOnInit() { }
  selectAndClose(add) {
    this.serviceProvider.modalCtrl.dismiss({
      data: add
    });
  }
  async goToAddNEwAddress() {
    this.serviceProvider.modalCtrl.dismiss();
    const modal = await this.serviceProvider.opneModal(AddaddressComponent);
    await modal.present();
  }
  closeModal() {
    this.serviceProvider.modalCtrl.dismiss();

  }
}
