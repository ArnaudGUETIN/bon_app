import { Component, NgZone } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { DeliveryService } from '../delivery.service';
import { SetlocationComponent } from '../setlocation/setlocation.component';
import { Router } from '@angular/router';
import { FilterComponent } from '../filter/filter.component';
declare var google;

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  lat: number;
  lng: number;
  map: any;
  block: any;
  street: any;
  building: any;
  flag: boolean;
  pickup: string;
  COMPLETE_ADDRESS: any;
  IMAGE_HEADER = '../../assets/offer.png';
  cardDetails: { text1: string; text2: string; image: string; }[];
  // tslint:disable-next-line: max-line-length
  itemDetails: ({ thumbnail: string; title: string; subTitle: string; imgSmall: string; text: string; rate: string; time: string; cost: string; slidesVertical?: any; slidesVerticalAvater?: any; })[];
  constructor(
    public geolocation: Geolocation,
    public serviceProvider: DeliveryService,
    public _ZONE: NgZone,
    public route: Router
  ) {
    this.serviceProvider.getCurrentLoaction();
    this.serviceProvider.COMPLETE_ADDRESS.subscribe(filter => {
      console.log(filter);
      this.COMPLETE_ADDRESS = filter;
    });
    this.cardDetails = this.serviceProvider.slidesVertical;
    this.itemDetails = this.serviceProvider.slidesHorizontal;
  }

  ionViewWillenter() {
    this.serviceProvider.getCurrentLoaction();
    console.log('view enetered');
  }
  async goToChangeLocation() {
    this.route.navigate(['search-address']);
  }
  async  openFilterModal() {
    const filterModal = await this.serviceProvider.opneModal(FilterComponent, '', 'customCss');
    await filterModal.present();
  }
  logScrolling(event) {
    // console.log(events)
    if (event.detail.scrollTop > 224.6666717529297) {
      document.getElementById('fixPosition').style.position = 'sticky';
      document.getElementById('fixPosition').style.top = '0px';
      document.getElementById('fixPosition').style.zIndex = '999';
      document.getElementById('fixPosition').style.background = 'white';

    } else if (event.detail.scrollTop < 224.6666717529297) {
      document.getElementById('fixPosition').style.position = 'relative';
      document.getElementById('fixPosition').style.top = '0px';
      document.getElementById('fixPosition').style.background = '#f4f5f8';

    }
  }
  goToProductPage(data: any) {
    this.route.navigate(['products', data]);
  }
  goToOffersPage() {
    this.route.navigate(['offers']);
    this.serviceProvider.checkCouponStatus(false);

  }
  gotoRestuarentsProducts(items: any) {
    console.log(items);
    this.route.navigate(['restroproducts', items]);
  }
}
