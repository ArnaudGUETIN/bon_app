/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DeliveryService } from '../delivery.service';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit {
  segmentValue: any;
  applyCoupon: any;

  constructor(
    public actvRoute: ActivatedRoute,
    public serviceProvider: DeliveryService
  ) {
    // this.actvRoute.params.subscribe(filterRoute => {
    //   this.serviceProvider.checkCouponStatus(filterRoute.value);
    // });
  }

  ngOnInit() {
  }
  segmentChanged(ev) {
    this.segmentValue = ev.detail.value;
  }
}
