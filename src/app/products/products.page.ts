/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeliveryService } from '../delivery.service';
import { FilterComponent } from '../filter/filter.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  prodData: any;
  title: boolean;
  // tslint:disable-next-line: max-line-length
  itemDetails: ({ thumbnail: string; title: string; subTitle: string; imgSmall: string; text: string; rate: string; time: string; cost: string; famousFor?: any; slidesVertical?: any; slidesVerticalAvater?: any; })[];

  constructor(
    public actvRoute: ActivatedRoute,
    public serviceProvider: DeliveryService,
    public route: Router
  ) {
    this.actvRoute.params.subscribe(filterdata => {
      this.prodData = filterdata;
      console.log(filterdata);
    });
    this.itemDetails = this.serviceProvider.slidesHorizontal;

  }

  ngOnInit() {
  }
  logScrolling(event) {
    console.log(event);
    if (event.detail.scrollTop > 147.1999969482422) {
      document.getElementById('productBar').style.position = 'sticky';
      document.getElementById('productBar').style.top = '56px';
      document.getElementById('productBar').style.zIndex = '999';
      document.getElementById('productBar').style.background = 'white';
      this.title = true;
    } else if (event.detail.scrollTop < 147.1999969482422) {
      document.getElementById('productBar').style.position = 'relative';
      document.getElementById('productBar').style.top = '0px';
      document.getElementById('productBar').style.background = '#f4f5f8';
      this.title = false;

    }
  }
  async  openFilterModal() {
    const filterModal = await this.serviceProvider.opneModal(FilterComponent, '', 'customCss');
    await filterModal.present();
  }
  gotoRestuarentsProducts(items: any) {
    console.log(items);
    this.route.navigate(['restroproducts', items]);
  }
}
