/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DeliveryService } from '../delivery.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-searchmodal',
  templateUrl: './searchmodal.component.html',
  styleUrls: ['./searchmodal.component.scss'],
})
export class SearchmodalComponent implements OnInit {

  searchInput = '';
  findArtist = 'Find your favorite artists';
  trySearching = 'Try searching or using a different spelling or keyword';
  searchData = [
    { image: '../../assets/food1.jpg', name: 'Idli Sanbhar', show: true, id: 0 },
    { image: '../../assets/food2.jpg', name: 'Poha', show: true, id: 1 },
    { image: '../../assets/food4.jpg', name: 'Roasted', show: true, id: 2 },
    { image: '../../assets/food5.jpg', name: 'Kadai Paneer', show: true, id: 3 },
    { image: '../../assets/food6.jpg', name: 'Daal Chawal', show: true, id: 4 },
    { image: '../../assets/food7.jpg', name: 'Fish Fry', show: true, id: 5 },
    { image: '../../assets/food8.jpg', name: 'Egg Bhurji', show: true, id: 6 },
    { image: '../../assets/food3.jpg', name: 'Omlett', show: true, id: 7 },
    { image: '../../assets/food5.jpg', name: 'Biryani', show: true, id: 8 },
    { image: '../../assets/food6.jpg', name: 'Dum Biryaani', show: true, id: 9 },
    { image: '../../assets/food1.jpg', name: 'Seekh Kabaab', show: true, id: 0 },
  ];
  constructor(
    public modalCtrl: ModalController,
    public serviceProvider: DeliveryService,
    public route: Router
  ) { }

  ngOnInit() { }
  dissmiss() {
    this.modalCtrl.dismiss();
  }
  artistSelected() {
    console.log(this.searchInput);
    this.route.navigate(['/tabs/hunger']);
    this.dissmiss();
  }
}
