/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';
import { MouseEvent } from '@agm/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-addaddress',
  templateUrl: './addaddress.component.html',
  styleUrls: ['./addaddress.component.scss'],
})
export class AddaddressComponent implements OnInit {

  pickupLat: any;
  pickupLng: any;
  markers = [];
  zoom = 8;
  houseDetail = '';
  landMark = '';
  public markerOptions = {
    origin: {
      animation: '\'DROP\'',
      label: 'origin',
    }
  };
  public renderOptions = {
    suppressMarkers: true,
  };
  Location: any;
  addDetails: { icon: string; name: string; }[];
  otherClicked: boolean;
  constructor(
    public serviceProvider: DeliveryService,
    public route: Router,
  ) {
    // this.serviceProvider.getCurrentLoaction();
    this.serviceProvider.originlatitude.subscribe(filterLat => {
      this.pickupLat = filterLat;
      console.log(this.pickupLat);
    });
    this.serviceProvider.originlongititude.subscribe(filterLng => {
      this.pickupLng = filterLng;
      console.log(this.pickupLat);
    });
    this.serviceProvider.COMPLETE_ADDRESS.subscribe(filter => {
      this.Location = filter;
      console.log(this.Location);
    });
    this.addDetails = this.serviceProvider.addresses;
  }
  ngOnInit() {
  }
  mapClicked($event: MouseEvent) {
    this.serviceProvider.mapClicked($event);
  }
  getCurrentLocation() {
    this.serviceProvider.getCurrentLoaction();
  }
  markerDragEnd($event: MouseEvent) {
    this.serviceProvider.markerDragEnd($event);
  }
  changeClass(value: { clicked: boolean; name: string }, index: number) {
    if (value.name === 'Home') {
      value.clicked = !value.clicked;
    } else if (value.name === 'Work') {
      value.clicked = !value.clicked;
    } else if (value.name === 'Other') {
      this.otherClicked = true;
    } else {
      value.clicked = false;
    }
  }
  closeOtherImput() {
    this.otherClicked = false;
  }
  goBack() {
    this.serviceProvider.modalCtrl.dismiss();
  }
  goToBilling() {
    this.serviceProvider.modalCtrl.dismiss();
  }
}
// tslint:disable-next-line: class-name
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
