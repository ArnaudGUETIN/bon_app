/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Injectable, NgZone } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { BehaviorSubject, Observable } from 'rxjs';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { MouseEvent } from '@agm/core';
declare var google;
@Injectable({
  providedIn: 'root'
})
export class DeliveryService {
  locatedCountry = 'IN';
  toggleCArt: boolean;
  // Obsevables filters
  public latitudeOrigin = new BehaviorSubject<any>('');
  originlatitude = this.latitudeOrigin.asObservable();
  public longitudeOrigin = new BehaviorSubject<any>('');
  originlongititude = this.longitudeOrigin.asObservable();

  // complete address details
  public ADDRESS_DETAIL = new BehaviorSubject<any>('');
  COMPLETE_ADDRESS = this.ADDRESS_DETAIL.asObservable();
  // cart Counter
  public totalCount = new BehaviorSubject<any>('');
  COUNT_TOTAL = this.totalCount.asObservable();

  public COUPON_COMING = new BehaviorSubject<any>('');
  COMING_COUPON = this.COUPON_COMING.asObservable();

  public NAME_USER = new BehaviorSubject<any>('');
  USER_NAME = this.NAME_USER.asObservable();

  public ADDRESS_USER = new BehaviorSubject<any>('');
  USER_ADDRESS = this.ADDRESS_USER.asObservable();

  public CONTACT_USER = new BehaviorSubject<any>('');
  USER_CONTACT = this.CONTACT_USER.asObservable();

  public LOC_USER = new BehaviorSubject<any>('');
  USER_LOC = this.LOC_USER.asObservable();

  public DESC_USER = new BehaviorSubject<any>('');
  USER_DESC = this.DESC_USER.asObservable();

  public IMAGE_USER = new BehaviorSubject<any>('');
  USER_IMAGE = this.IMAGE_USER.asObservable();
  public inputCount = new BehaviorSubject<any>('');
  countInput = this.inputCount.asObservable();

  slidesVertical = [
    { text1: 'UPTO 70% OFF', text2: 'GURU WHICH TASTE THE BEST', image: '../../assets/food1.jpg' },
    { text1: 'GET 40% OFF', text2: 'FROM YOUR CHOICE', image: '../../assets/food8.jpg' },
    { text1: 'FLAT 45% OFF', text2: 'FROM HOME', image: '../../assets/food5.jpg' },
    { text1: 'ABSOLUTELY 21% OFF', text2: 'GHAR KA KHANA', image: '../../assets/food18.jpg' },
    { text1: 'UPTO 30% OFF', text2: 'ARARIA FOOD HOUSE', image: '../../assets/food12.jpg' },
    { text1: 'UPTO 10% OFF', text2: 'EXCLUSIVE ROASTED CHICKEN', image: '../../assets/food11.jpg' },
    { text1: 'UPTO 90% OFF', text2: 'BS YAHI BACHA HAI', image: '../../assets/food3.jpg' },
    { text1: 'UPTO 30% OFF', text2: 'YOUR TASTE', image: '../../assets/food19.jpg' },
    { text1: 'UPTO 66% OFF', text2: 'SHADMAN COOKIES', image: '../../assets/food17.jpg' },
    { text1: 'UPTO 60% OFF', text2: 'FOOD FROM HOME', image: '../../assets/food10.jpg' },
    { text1: 'UPTO 20% OFF', text2: 'TASTE MEI BEST', image: '../../assets/food9.jpg' },
    { text1: 'UPTO 15% OFF', text2: 'OUR FOOD', image: '../../assets/food12.jpg' },
  ];
  previousOrders = [
    { shopImg: '../../assets/food1.jpg', shopName: 'Bihar Ki Shaan', totalItem: '2', totalPrice: 120, item: 'ITEMS', order: 'ORDERED ON', orderDate: '22.05.2019 at 09:30 AM', itemList: '1 X Aaloo Bhurji, 1 X Daal fry, 2 X Chicken Roasted + 2 X Chapaati', location: 'HSR Layout,Banglore', rating: '4.5' },
    { shopImg: '../../assets/food2.jpg', shopName: 'Calcutta Biryani', totalItem: '../../assets/food1.jpg', totalPrice: 120, item: 'ITEMS', order: 'ORDERED ON', orderDate: '22.05.2019 at 02:30 AM', itemList: '1 X Aaloo Bhurji, 1 X Daal fry, 2 X Chicken Roasted + 2 X Chapaati', location: 'HSR Layout,Banglore', rating: '3.5' },
    { shopImg: '../../assets/food3.jpg', shopName: 'Jaipur Dhaaba', totalItem: '4', totalPrice: 120, item: 'ITEMS', order: 'ORDERED ON', orderDate: '22.05.2019 at 09:30 PM', itemList: '1 X Aaloo Bhurji, 1 X Daal fry, 2 X Chicken Roasted + 2 X Chapaati', location: 'HSR Layout,Banglore', rating: '2.5' },
    { shopImg: '../../assets/food4.jpg', shopName: 'Kota Kachori', totalItem: '5', totalPrice: 120, item: 'ITEMS', order: 'ORDERED ON', orderDate: '22.05.2019 at 04:30 PM', itemList: '1 X Aaloo Bhurji, 1 X Daal fry, 2 X Chicken Roasted + 2 X Chapaati', location: 'HSR Layout,Banglore', rating: '4.5' },
    { shopImg: '../../assets/food5.jpg', shopName: 'Kota Kachori', totalItem: '3', totalPrice: 120, item: 'ITEMS', order: 'ORDERED ON', orderDate: '22.05.2019 at 09:30 AM', itemList: '1 X Aaloo Bhurji, 1 X Daal fry, 2 X Chicken Roasted + 2 X Chapaati', location: 'HSR Layout,Banglore', rating: '4.5' },
    { shopImg: '../../assets/food6.jpg', shopName: 'GURU WHICH TASTE THE BEST', totalItem: '4', totalPrice: 120, item: 'ITEMS', order: 'ORDERED ON', orderDate: '22.05.2019 at 19:30 PM', itemList: '1 X Aaloo Bhurji, 1 X Daal fry, 2 X Chicken Roasted + 2 X Chapaati', location: 'HSR Layout,Banglore', rating: '1.5' },
    { shopImg: '../../assets/food7.jpg', shopName: 'GURU WHICH TASTE THE BEST', totalItem: '1', totalPrice: 120, item: 'ITEMS', order: 'ORDERED ON', orderDate: '22.05.2019 at 09:30 AM', itemList: '1 X Aaloo Bhurji, 1 X Daal fry, 2 X Chicken Roasted + 2 X Chapaati', location: 'HSR Layout,Banglore', rating: '5' },
    { shopImg: '../../assets/food8.jpg', shopName: 'GURU WHICH TASTE THE BEST', totalItem: '7', totalPrice: 120, item: 'ITEMS', order: 'ORDERED ON', orderDate: '22.05.2019 at 09:30 AM', itemList: '1 X Aaloo Bhurji, 1 X Daal fry, 2 X Chicken Roasted + 2 X Chapaati', location: 'HSR Layout,Banglore', rating: '4' },
    { shopImg: '../../assets/food9.jpg', shopName: 'GURU WHICH TASTE THE BEST', totalItem: '4', totalPrice: 120, item: 'ITEMS', order: 'ORDERED ON', orderDate: '22.05.2019 at 09:30 PM', itemList: '1 X Aaloo Bhurji, 1 X Daal fry, 2 X Chicken Roasted + 2 X Chapaati', location: 'HSR Layout,Banglore', rating: '2.5' },
    { shopImg: '../../assets/food10.jpg', shopName: 'GURU WHICH TASTE THE BEST', totalItem: '6', totalPrice: 120, item: 'ITEMS', order: 'ORDERED ON', orderDate: '22.05.2019 at 09:30 AM', itemList: '1 X Aaloo Bhurji, 1 X Daal fry, 2 X Chicken Roasted + 2 X Chapaati', location: 'HSR Layout,Banglore', rating: '4.5' },
  ];
  offerSlides = [
    {
      head: 'Top Offers',
      subHead: 'Top offers specially for you',
      slides: [
        {
          text1: 'UPTO 70% OFF', image: '../../assets/food1.jpg', rating: '3.9', time: '42 MINS', price: '249'
        },
        {
          text1: 'GET 40% OFF', image: '../../assets/food8.jpg', rating: '3.9', time: '42 MINS', price: '249'
        },
        {
          text1: 'FLAT 45% OFF', image: '../../assets/food5.jpg', rating: '3.9', time: '42 MINS', price: '249'
        },
        {
          text1: 'ABSOLUTELY 21% OFF', image: '../../assets/food18.jpg', rating: '3.9', time: '42 MINS', price: '249'
        },
        {
          text1: 'UPTO 30% OFF', image: '../../assets/food12.jpg', rating: '3.9', time: '42 MINS', price: '249'
        },
        {
          text1: 'UPTO 10% OFF', image: '../../assets/food11.jpg', rating: '3.9', time: '42 MINS', price: '249'
        },
        {
          text1: 'UPTO 90% OFF', image: '../../assets/food3.jpg', rating: '3.9', time: '42 MINS', price: '249'
        },
        {
          text1: 'UPTO 30% OFF', image: '../../assets/food19.jpg', rating: '3.9', time: '42 MINS', price: '249'
        },
        {
          text1: 'UPTO 66% OFF', image: '../../assets/food17.jpg', rating: '3.9', time: '42 MINS', price: '249'
        },
        {
          text1: 'UPTO 60% OFF', image: '../../assets/food10.jpg', rating: '3.9', time: '42 MINS', price: '249'
        },
        {
          text1: 'UPTO 20% OFF', image: '../../assets/food9.jpg', rating: '3.9', time: '42 MINS', price: '249'
        },
        {
          text1: 'UPTO 15% OFF', image: '../../assets/food12.jpg', rating: '3.9', time: '42 MINS', price: '249'
        },
      ]
    },
    {
      head: 'Great Brands, Great deals',
      subHead: 'Deals & Discounts for you',
      slides: [
        {
          text1: 'UPTO 70% OFF', text2: 'GURU WHICH TASTE THE BEST', image: '../../assets/food1.jpg', imgSmall: '../../assets/offer.png',
          text: '40% OFF | Use coupon ENAPPD40',
        },
        {
          text1: 'GET 40% OFF', text2: 'FROM YOUR CHOICE', image: '../../assets/food8.jpg', imgSmall: '../../assets/offer.png',
          text: '40% OFF | Use coupon ENAPPD40',
        },
        {
          text1: 'FLAT 45% OFF', text2: 'FROM HOME', image: '../../assets/food5.jpg', imgSmall: '../../assets/offer.png',
          text: '40% OFF | Use coupon ENAPPD40',
        },
        {
          text1: 'ABSOLUTELY 21% OFF', text2: 'GHAR KA KHANA', image: '../../assets/food18.jpg', imgSmall: '../../assets/offer.png',
          text: '40% OFF | Use coupon ENAPPD40',
        },
        {
          text1: 'UPTO 30% OFF', text2: 'ARARIA FOOD HOUSE', image: '../../assets/food12.jpg', imgSmall: '../../assets/offer.png',
          text: '40% OFF | Use coupon ENAPPD40',
        },
        {
          text1: 'UPTO 10% OFF', text2: 'EXCLUSIVE ROASTED CHICKEN', image: '../../assets/food11.jpg', imgSmall: '../../assets/offer.png',
          text: '40% OFF | Use coupon ENAPPD40',
        },
        {
          text1: 'UPTO 90% OFF', text2: 'BS YAHI BACHA HAI', image: '../../assets/food3.jpg', imgSmall: '../../assets/offer.png',
          text: '40% OFF | Use coupon ENAPPD40',
        },
        {
          text1: 'UPTO 30% OFF', text2: 'YOUR TASTE', image: '../../assets/food19.jpg', imgSmall: '../../assets/offer.png',
          text: '40% OFF | Use coupon ENAPPD40',
        },
        {
          text1: 'UPTO 66% OFF', text2: 'SHADMAN COOKIES', image: '../../assets/food17.jpg', imgSmall: '../../assets/offer.png',
          text: '40% OFF | Use coupon ENAPPD40',
        },
        {
          text1: 'UPTO 60% OFF', text2: 'FOOD FROM HOME', image: '../../assets/food10.jpg', imgSmall: '../../assets/offer.png',
          text: '40% OFF | Use coupon ENAPPD40',
        },
        {
          text1: 'UPTO 20% OFF', text2: 'TASTE MEI BEST', image: '../../assets/food9.jpg', imgSmall: '../../assets/offer.png',
          text: '40% OFF | Use coupon ENAPPD40',
        },
        {
          text1: 'UPTO 15% OFF', text2: 'OUR FOOD', image: '../../assets/food12.jpg', imgSmall: '../../assets/offer.png',
          text: '40% OFF | Use coupon ENAPPD40',
        },
      ]
    },
  ];
  slidesHorizontal = [
    {
      thumbnail: '../../assets/food1.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food11.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      famousFor: [
        // tslint:disable-next-line: max-line-length
        { text1: 'North INDIAN', text2: '28 MINS', image: '../../assets/food19.jpg' }, { text1: 'PASTA', text2: '30 MINS', image: '../../assets/food19.jpg' }, { text1: 'BEVERAGES', text2: '21 MINS', image: '../../assets/food5.jpg' }, { text1: 'AL-HAQ', text2: '4 MINS', image: '../../assets/food18.jpg' },
        // tslint:disable-next-line: max-line-length
        { text1: 'FAST FOOD', text2: '12 MINS', image: '../../assets/food20.jpg' }, { text1: 'BIRYANI', text2: '37 MINS', image: '../../assets/food16.jpg' }, { text1: 'DESSERTS', text2: '11 MINS', image: '../../assets/food3.jpg' }, { text1: 'AL-BEK', text2: '21 MINS', image: '../../assets/food19.jpg' },
        // tslint:disable-next-line: max-line-length
        { text1: 'PIZZA', text2: '48 MINS', image: '../../assets/food12.jpg' }, { text1: 'KAWABS', text2: '28 MINS', image: '../../assets/food6.jpg' }, { text1: 'CHINESE', text2: '32 MINS', image: '../../assets/food9.jpg' }, { text1: 'AL-SHAD', text2: '32 MINS', image: '../../assets/food12.jpg' },
      ]
    },
    {
      thumbnail: '../../assets/food12.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food13.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food14.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food15.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food16.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food17.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      slidesVertical: [
        // tslint:disable-next-line: max-line-length
        { text1: 'McDONALD\'S', text2: '28 MINS', image: '../../assets/food1.jpg' }, { text1: 'DOMINO\'S PIZZA', text2: '30 MINS', image: '../../assets/food8.jpg' }, { text1: 'RICE BOWL', text2: '21 MINS', image: '../../assets/food5.jpg' }, { text1: 'AL-HAQ', text2: '4 MINS', image: '../../assets/food18.jpg' },
        // tslint:disable-next-line: max-line-length
        { text1: 'BURGER KING', text2: '12 MINS', image: '../../assets/food12.jpg' }, { text1: 'TRUFFLES', text2: '37 MINS', image: '../../assets/food11.jpg' }, { text1: 'HOMELY', text2: '11 MINS', image: '../../assets/food3.jpg' }, { text1: 'AL-BEK', text2: '21 MINS', image: '../../assets/food19.jpg' },
        // tslint:disable-next-line: max-line-length
        { text1: 'KFC', text2: '48 MINS', image: '../../assets/food17.jpg' }, { text1: 'SUBWAY', text2: '28 MINS', image: '../../assets/food10.jpg' }, { text1: 'BBQ', text2: '32 MINS', image: '../../assets/food9.jpg' }, { text1: 'AL-SHAD', text2: '32 MINS', image: '../../assets/food12.jpg' },
      ]
    },
    {
      thumbnail: '../../assets/food18.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food1.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food2.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food3.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food4.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food5.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food6.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      slidesVerticalAvater: [
        // tslint:disable-next-line: max-line-length
        { text1: 'EK CUP COFFEE', text2: 'GURU WHICH TASTE THE BEST', image: '../../assets/food1.jpg' }, { text1: 'OUNJABI SANDWICHES', text2: 'FROM YOUR CHOICE', image: '../../assets/food8.jpg' }, { text1: 'SABKA VIKAS', text2: 'FROM HOME', image: '../../assets/food5.jpg' }, { text1: 'ABSOLUTELY 21% OFF', text2: 'GHAR KA KHANA', image: '../../assets/food18.jpg' },
        // tslint:disable-next-line: max-line-length
        { text1: 'GHAR KA KHANA', text2: 'ARARIA FOOD HOUSE', image: '../../assets/food12.jpg' }, { text1: 'LUKNNOW MIRROR', text2: 'EXCLUSIVE ROASTED CHICKEN', image: '../../assets/food11.jpg' }, { text1: 'ENAPPD', text2: 'BS YAHI BACHA HAI', image: '../../assets/food3.jpg' }, { text1: 'UPTO 30% OFF', text2: 'YOUR TASTE', image: '../../assets/food19.jpg' },
        // tslint:disable-next-line: max-line-length
        { text1: 'ADRAK LEHSUN', text2: 'SHADMAN COOKIES', image: '../../assets/food17.jpg' }, { text1: 'SABKA SATH', text2: 'FOOD FROM HOME', image: '../../assets/food10.jpg' }, { text1: 'UPTO 20% OFF', text2: 'TASTE MEI BEST', image: '../../assets/food9.jpg' }, { text1: 'UPTO 15% OFF', text2: 'OUR FOOD', image: '../../assets/food12.jpg' },
      ]
    },
    {
      thumbnail: '../../assets/food7.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food8.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food9.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food10.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food11.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
    {
      thumbnail: '../../assets/food12.jpg',
      title: 'Burger King',
      subTitle: 'American, Fast Food',
      imgSmall: '../../assets/offer.png',
      text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two'
    },
  ];
  radios = [
    { name: 'Relevance', value: 'first' },
    { name: 'Cost For Two', value: 'sec' },
    { name: 'Delivery Time', value: 'thirs' },
    { name: 'Rating', value: 'fourth' },
  ];
  offers = [
    { name: 'My Favorites', value: 'first' },
    { name: 'Offer', value: 'sec' },
    { name: 'Pure Veg', value: 'thirs' },
  ];
  restorentProducts = [
    {
      head: 'Your Previous orders',
      id: 'orders',
      repeatMenu: [
        { restroName: 'Maa di special Chiken dum Biryani', foodType: 'Main Course', cost: '100', inputValue: 1, clicked: false },
        { restroName: 'Maa di special Chiken dum Biryani', foodType: 'Main Course', cost: '200', inputValue: 1, clicked: false },
        { restroName: 'Maa di special Chiken dum Biryani', foodType: 'Main Course', cost: '102', inputValue: 1, clicked: false },
        { restroName: 'Maa di special Chiken dum Biryani', foodType: 'Main Course', cost: '120', inputValue: 1, clicked: false },
        { restroName: 'Maa di special Chiken dum Biryani', foodType: 'Main Course', cost: '110', inputValue: 1, clicked: false }
      ]
    },
    {
      head: 'Recommended',
      id: 'Recommended',
      menus: [
        { image: '../../assets/food14.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food13.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food10.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food11.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food3.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food4.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food5.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food6.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food7.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food8.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food9.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food10.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food11.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food12.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food13.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food14.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food15.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food16.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food17.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food18.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food19.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food1.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food12.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food13.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food14.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food15.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food18.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food11.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food15.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
        { image: '../../assets/food12.jpg', sellerType: 'Top Sellers', name: 'Shadman\'s Choice', price: '101', inputValue: 1, clicked: false },
      ]
    },
    {
      head: 'Some Enappd Special',
      id: 'special',
      itemView: [
        { dishName: 'The developer\'s Coded Biryani', dishPrice: '329', contents: 'Developers Biryani- Flask + Veg Puff + Chicken Roated + Mutton Chaanp + Raita & Lassi', inputValue: 1, clicked: false },
        { dishName: 'The developer\'s Coded Biryani', dishPrice: '329', contents: 'Developers Biryani- Flask + Veg Puff + Chicken Roated + Mutton Chaanp + Raita & Lassi', inputValue: 1, clicked: false },
        { dishName: 'The developer\'s Coded Biryani', dishPrice: '329', contents: 'Developers Biryani- Flask + Veg Puff + Chicken Roated + Mutton Chaanp + Raita & Lassi', inputValue: 1, clicked: false },
        { dishName: 'The developer\'s Coded Biryani', dishPrice: '329', contents: 'Developers Biryani- Flask + Veg Puff + Chicken Roated + Mutton Chaanp + Raita & Lassi', inputValue: 1, clicked: false },
        { dishName: 'The developer\'s Coded Biryani', dishPrice: '329', contents: 'Developers Biryani- Flask + Veg Puff + Chicken Roated + Mutton Chaanp + Raita & Lassi', inputValue: 1, clicked: false },
        { dishName: 'The developer\'s Coded Biryani', dishPrice: '329', contents: 'Developers Biryani- Flask + Veg Puff + Chicken Roated + Mutton Chaanp + Raita & Lassi', inputValue: 1, clicked: false },
        { dishName: 'The developer\'s Coded Biryani', dishPrice: '329', contents: 'Developers Biryani- Flask + Veg Puff + Chicken Roated + Mutton Chaanp + Raita & Lassi', inputValue: 1, clicked: false },
        { dishName: 'The developer\'s Coded Biryani', dishPrice: '329', contents: 'Developers Biryani- Flask + Veg Puff + Chicken Roated + Mutton Chaanp + Raita & Lassi', inputValue: 1, clicked: false },
        { dishName: 'The developer\'s Coded Biryani', dishPrice: '329', contents: 'Developers Biryani- Flask + Veg Puff + Chicken Roated + Mutton Chaanp + Raita & Lassi', inputValue: 1, clicked: false },
        { dishName: 'The developer\'s Coded Biryani', dishPrice: '329', contents: 'Developers Biryani- Flask + Veg Puff + Chicken Roated + Mutton Chaanp + Raita & Lassi', inputValue: 1, clicked: false },
      ]
    }, {
      head: 'Top Sellers',
      id: 'seller',
      itemView: [
        { dishName: 'Classi Masala Chiken curry', dishPrice: '329', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
        { dishName: 'Classi Masala Chiken curry', dishPrice: '329', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
        { dishName: 'Classi Masala Chiken curry', dishPrice: '329', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
        { dishName: 'Classi Masala Chiken curry', dishPrice: '329', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
        { dishName: 'Classi Masala Chiken curry', dishPrice: '329', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
        { dishName: 'Classi Masala Chiken curry', dishPrice: '329', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
        { dishName: 'Classi Masala Chiken curry', dishPrice: '329', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
        { dishName: 'Classi Masala Chiken curry', dishPrice: '329', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
        { dishName: 'Classi Masala Chiken curry', dishPrice: '329', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
        { dishName: 'Classi Masala Chiken curry', dishPrice: '329', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
      ]
    },
    {
      head: 'Hot Beverages',
      id: 'hot',
      accordianItem: [
        {
          dishName: 'Chiken Flask', dishItem: '24 items:', items: 'Dilliwali Dum Biryani,Bhuna Masaala tikka...and more', acordian: false, id: 'Flask',
          accordian: [
            { dishName: 'Cutting Ghobhi Masaala', dishPrice: '654', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Cutting Ghobhi Masaala', dishPrice: '654', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Cutting Ghobhi Masaala', dishPrice: '654', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Cutting Ghobhi Masaala', dishPrice: '654', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Cutting Ghobhi Masaala', dishPrice: '654', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Cutting Ghobhi Masaala', dishPrice: '654', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Cutting Ghobhi Masaala', dishPrice: '654', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Cutting Ghobhi Masaala', dishPrice: '654', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Cutting Ghobhi Masaala', dishPrice: '654', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Cutting Ghobhi Masaala', dishPrice: '654', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
          ]
        },
        {
          dishName: 'Vegetable Soup', dishItem: '23 items:', items: 'Dilliwali Dum Biryani,Bhuna Masaala tikka...and more', acordian: false, id: 'Soup',
          accordian: [
            { dishName: 'Ginger Lemon Mix Soup', dishPrice: '102', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Ginger Lemon Mix Soup', dishPrice: '102', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Ginger Lemon Mix Soup', dishPrice: '102', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Ginger Lemon Mix Soup', dishPrice: '102', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Ginger Lemon Mix Soup', dishPrice: '102', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Ginger Lemon Mix Soup', dishPrice: '102', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Ginger Lemon Mix Soup', dishPrice: '102', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Ginger Lemon Mix Soup', dishPrice: '102', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Ginger Lemon Mix Soup', dishPrice: '102', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Ginger Lemon Mix Soup', dishPrice: '102', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
          ]
        },
        {
          dishName: 'Kalmi Kabab', dishItem: '15 items:', items: 'Dilliwali Dum Biryani,Bhuna Masaala tikka...and more', acordian: false, id: 'Kabab',
          accordian: [
            { dishName: 'Punjabi Daal Tadka', dishPrice: '189', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Punjabi Daal Tadka', dishPrice: '189', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Punjabi Daal Tadka', dishPrice: '189', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Punjabi Daal Tadka', dishPrice: '189', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Punjabi Daal Tadka', dishPrice: '189', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Punjabi Daal Tadka', dishPrice: '189', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Punjabi Daal Tadka', dishPrice: '189', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Punjabi Daal Tadka', dishPrice: '189', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Punjabi Daal Tadka', dishPrice: '189', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Punjabi Daal Tadka', dishPrice: '189', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
          ]
        },
        {
          dishName: 'Kalmi Biryani', dishItem: '15 items:', items: 'Dilliwali Dum Biryani,Bhuna Masaala tikka...and more', acordian: false, id: 'Kalmi',
          accordian: [
            { dishName: 'Hydrabadi Dum Biryani', dishPrice: '120', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Hydrabadi Dum Biryani', dishPrice: '120', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Hydrabadi Dum Biryani', dishPrice: '120', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Hydrabadi Dum Biryani', dishPrice: '120', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Hydrabadi Dum Biryani', dishPrice: '120', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Hydrabadi Dum Biryani', dishPrice: '120', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Hydrabadi Dum Biryani', dishPrice: '120', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Hydrabadi Dum Biryani', dishPrice: '120', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Hydrabadi Dum Biryani', dishPrice: '120', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
            { dishName: 'Hydrabadi Dum Biryani', dishPrice: '120', sellerType: 'BESTSELLER', inputValue: 1, clicked: false },
          ]
        },
      ]
    },
  ];

  checkboxes = [
    { name: 'American', value: 'first' },
    { name: 'Andhra', value: 'sec' },
    { name: 'Arabian', value: 'thirs' },
    { name: 'Asian', value: 'fourth' },
    { name: 'Lenbanon', value: 'fourth' },
    { name: 'Assamese', value: 'fourth' },
    { name: 'Australian', value: 'fourth' },
    { name: 'Bakery', value: 'fourth' },
    { name: 'Barbecue', value: 'fourth' },
    { name: 'Bihari', value: 'fourth' },
    { name: 'Bevarages', value: 'fourth' },
    { name: 'Biryani', value: 'fourth' },
    { name: 'British', value: 'fourth' },
    { name: 'Burmese', value: 'fourth' },
    { name: 'Cafe', value: 'fourth' },
    { name: 'Chaat', value: 'fourth' },
    { name: 'Vhettinad', value: 'fourth' },
    { name: 'Chinese', value: 'fourth' },
    { name: 'Coastal', value: 'fourth' },
    { name: 'Combo', value: 'fourth' },
  ];
  menuList = [
    { menu: 'Kalmi Biryani', quantity: '23', id: 'hot' },
    { menu: 'Kalmi Kabab', quantity: '26', id: 'seller' },
    { menu: 'Vegetable Soup', quantity: '21', id: 'special' },
    { menu: 'Chiken Flask', quantity: '28', id: 'Recommended' },
    { menu: 'Chiken Tikka\'s', quantity: '28', id: 'orders' }
  ];
  addresses = [
    { icon: 'home', name: 'Home', clicked: false }, { icon: 'briefcase', name: 'Work', clicked: false }, { icon: 'pin', name: 'Other', clicked: false }];

  addressList = [
    { name: 'Bangaluru', add: '312,3rd Floor,4th Cross Kormangla....', time: '42 MINS' },
    { name: 'Mumbai', add: '312,3rd Floor,4th Cross Kormangla....', time: '32 MINS' },
    { name: 'Chennai', add: '312,3rd Floor,4th Cross Kormangla....', time: '22 MINS' },
    { name: 'Araria', add: '312,3rd Floor,4th Cross Kormangla....', time: '12 MINS' },
    { name: 'New Delhi', add: '312,3rd Floor,4th Cross Kormangla....', time: '22 MINS' },
    { name: 'Manali', add: '312,3rd Floor,4th Cross Kormangla....', time: '52 MINS' },
  ];
  payments = [
    {
      head: 'preferred payment',
      items: [
        {
          thumbRight: '../../assets/phonePay.jpg',
          textLeft: 'link account',
          text: 'PhonePe',
          id: 'PhonePe',
        }
      ]
    },
    {
      head: 'upi',
      items: [
        {
          thumbRight: '../../assets/google.png',
          text: 'google pay',
          iconLeft: true,
          clicked: false,
          id: 'google'
        },
        {
          icon: 'add',
          text: 'add new upi id',
          para: 'You need to have a registered UPI id'
        }
      ]
    },
    {
      head: 'wallets',
      items: [
        {
          thumbRight: '../../assets/amazon.png',
          text: 'amazon pay',
          textLeft: 'link account',
        },
        {
          thumbRight: '../../assets/paytm.png',
          text: 'paytm',
          textLeft: 'link account',
        },
        {
          thumbRight: '../../assets/freechsrge.jpg',
          text: 'freecharge',
          textLeft: 'link account',
        }
      ]
    },
    {
      head: 'credit / debit card',
      items: [
        {
          thumbRight: '../../assets/visa.png',
          text: '123-XXXXXXXXXX-456',
          iconLeft: true,
          clicked: false,
          id: 'visa'
        },
        {
          icon: 'add',
          text: 'add new card',
          para: 'Save and pay via cards'
        }
      ]
    },
    {
      head: 'Pay on delivery',
      items: [
        {
          thumbRight: '../../assets/cash.png',
          text: 'Cash',
          iconLeft: true,
          clicked: false,
          id: 'cash',
          para: 'Please keep exact change at the delivery time to help us serve you best.'
        }
      ]
    }
  ];
  paymentOffer = [
    {
      head: 'AVAILABLE COUPONS',
      coupons: [
        { coupImg: '../../assets/amazon.png', code: 'ENAPPD@325', couTitle: 'Get assured cashback upto Rs.10 to Rs.400', coupDesc: 'Use code ENAPPD@325 & get assured cashback between Rs.10 to Rs.400 on your all orders at enappd.com' },
        { coupImg: '../../assets/phonePay.jpg', code: 'ENAPPD25', couTitle: 'Get assured cashback upto Rs.10 to Rs.400', coupDesc: 'Use code ENAPPD@325 & get assured cashback between Rs.10 to Rs.400 on your all orders at enappd.com' },
        { coupImg: '../../assets/paytm.png', code: 'ENAPPST25', couTitle: 'Get assured cashback upto Rs.1000 to Rs.4000', coupDesc: 'Use code ENAPPD@325 & get assured cashback between Rs.10 to Rs.400 on your all orders at enappd.com' },
        { coupImg: '../../assets/google.png', code: 'ENAPPD@125', couTitle: 'Get assured cashback upto Rs.50 to Rs.200', coupDesc: 'Use code ENAPPD@325 & get assured cashback between Rs.10 to Rs.400 on your all orders at enappd.com' },
      ]
    },
    {
      head: 'PAYMENT OFFERS',
      coupons: [
        { coupImg: '../../assets/cash.png', code: 'ENAPPD@325', couTitle: 'Get 20% SUPERCASH USING ENAPPDPAY ', coupDesc: 'Use code ENAPPD@325 & get assured cashback between Rs.10 to Rs.400 on your all orders at enappd.com' },
        { coupImg: '../../assets/phonePay.jpg', code: 'ENAPPD25', couTitle: 'Get 20% SUPERCASH USING ENAPPDPAY ', coupDesc: 'Use code ENAPPD@325 & get assured cashback between Rs.10 to Rs.400 on your all orders at enappd.com' },
        { coupImg: '../../assets/paytm.png', code: 'ENAPPST25', couTitle: 'Get 20% SUPERCASH USING ENAPPDPAY ', coupDesc: 'Use code ENAPPD@325 & get assured cashback between Rs.10 to Rs.400 on your all orders at enappd.com' },
        { coupImg: '../../assets/enappd_logo.png', code: 'ENAPPD@125', couTitle: 'Get assured cashback upto Rs.50 to Rs.200', coupDesc: 'Use code ENAPPD@325 & get assured cashback between Rs.10 to Rs.400 on your all orders at enappd.com' },
      ]
    }
  ];
  needHelp = {
    'Shipping and Delivery': [
      { 'How log does shipping take?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'How can I track my order?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'How much does shipping cost?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'Where does my order ship from?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'How do I change my shipping address?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' }
    ],
    'Returns and Refunds': [
      { 'How do I return a product?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'Can I exchange an item?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'How do I cancel my order?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'What\'s the status of my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'I cancelled my order. How will I receive my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' }
    ],
    'Payment, Pricing & Promotions': [
      { 'How do I return a product?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'Can I exchange an item?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'How do I cancel my order?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'What\'s the status of my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'I cancelled my order. How will I receive my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'What\'s the status of my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'I cancelled my order. How will I receive my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' }
    ],
    Orders: [
      { 'What\'s the status of my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'I cancelled my order. How will I receive my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'What\'s the status of my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'I cancelled my order. How will I receive my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' }
    ],
    'Managing Your Account': [
      { 'How do I return a product?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'Can I exchange an item?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'How do I cancel my order?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'What\'s the status of my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'I cancelled my order. How will I receive my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' }
    ],
    'User Feedback': [
      { 'How do I return a product?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'Can I exchange an item?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'How do I cancel my order?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'What\'s the status of my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'I cancelled my order. How will I receive my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' }
    ],
    'Customer Support': [
      { 'How do I return a product?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'Can I exchange an item?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'How do I cancel my order?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'What\'s the status of my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' },
      { 'I cancelled my order. How will I receive my refund?': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit sed lacus non condimentum. Sed sapien augue, ornare non eros eu, bibendum pulvinar purus. Aenean eu blandit elit, quis tincidunt turpis.' }
    ]
  };
  artistData = [
    {
      image: '../../assets/food1.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Rajasthani Thaali',
      imgSmall: '../../assets/offer.png',
      id: 0
    },
    {
      image: '../../assets/food2.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'English Thaali',
      imgSmall: '../../assets/offer.png', id
        : 1
    },
    {
      image: '../../assets/food4.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Punjabi Thaali',
      imgSmall: '../../assets/offer.png', id: 2
    },
    {
      image: '../../assets/food5.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Tamil Thaali',
      imgSmall: '../../assets/offer.png', id: 3
    },
    {
      image: '../../assets/food6.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Telgu Thaali',
      imgSmall: '../../assets/offer.png', id: 4
    },
    {
      image: '../../assets/food7.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Malyalam Thaali',
      imgSmall: '../../assets/offer.png',
      id: 5
    },
    {
      image: '../../assets/food8.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Marathi Thaali',
      imgSmall: '../../assets/offer.png',
      id: 6
    },
    {
      image: '../../assets/food3.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Gujrati Thaali',
      imgSmall: '../../assets/offer.png',
      id: 7
    },
    {
      image: '../../assets/food5.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Bengali Thaali',
      imgSmall: '../../assets/offer.png',
      id: 8
    },
    {
      image: '../../assets/food6.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Kannada Thaali',
      imgSmall: '../../assets/offer.png',
      id: 9
    },
    {
      image: '../../assets/food1.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Hindi Thaali',
      imgSmall: '../../assets/offer.png',
      id: 0
    },
    {
      image: '../../assets/food2.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'English Thaali',
      imgSmall: '../../assets/offer.png',
      id: 1
    },
    {
      image: '../../assets/food4.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Punjabi Thaali',
      imgSmall: '../../assets/offer.png',
      id: 2
    },
    {
      image: '../../assets/food5.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Tamil Thaali',
      imgSmall: '../../assets/offer.png',
      id: 3
    },
    {
      image: '../../assets/food6.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Telgu Thaali',
      imgSmall: '../../assets/offer.png',
      id: 4
    },
    {
      image: '../../assets/food7.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Malyalam Thaali',
      imgSmall: '../../assets/offer.png',
      id: 5
    },
    {
      image: '../../assets/food8.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Marathi Thaali',
      imgSmall: '../../assets/offer.png',
      id: 6
    },
    {
      image: '../../assets/food3.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Gujrati Thaali',
      imgSmall: '../../assets/offer.png',
      id: 7
    },
    {
      image: '../../assets/food5.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Bengali Thaali',

    },
    {
      image: '../../assets/food6.jpg', text: '40% OFF | Use coupon ENAPPD40',
      rate: ' 4.4',
      time: '30 mins',
      cost: '450',
      for: 'For two',
      subTitle: 'American, Fast Food',
      title: 'Kannada Thaali',

    },
  ];
  slideselectArtists = [
    { image: '../../assets/food1.jpg', name: 'Aman Hayer', imgSmall: '../../assets/offer.png', id: 0 },
    { image: '../../assets/food2.jpg', name: 'Nikhil sachdeva', show: false, id: 1 },
    { image: '../../assets/food4.jpg', name: 'Arijit', show: false, id: 2 },
    { image: '../../assets/food5.jpg', name: 'Sonu Nigam', show: false, id: 3 },
    { image: '../../assets/food6.jpg', name: 'Neha Kakkar', show: false, id: 4 },
    { image: '../../assets/food7.jpg', name: 'JoJO', show: false, id: 5 },
    { image: '../../assets/food8.jpg', name: 'Arman Malick', show: false, id: 6 },
    { image: '../../assets/food9.jpg', name: 'Amaal Malick', show: false, id: 7 },
    { image: '../../assets/food10.jpg', name: 'Silva d\'souza', show: false, id: 8 },
    { image: '../../assets/food11.jpg', name: 'Amit Trivedi', show: false, id: 9 },
    { image: '../../assets/food12.jpg', name: 'Pit bull', show: false, id: 0 },
    { image: '../../assets/food13.jpg', name: 'Lady Gaaga', show: false, id: 1 },
    { image: '../../assets/food14.jpg', name: 'Rihan', show: false, id: 2 },
    { image: '../../assets/food15.jpg', name: 'Taylor Swift', show: false, id: 3 },
    { image: '../../assets/food16.jpg', name: 'Selena Gomez', show: false, id: 4 },
    { image: '../../assets/food17.jpg', name: 'Justin Bieber', show: false, id: 5 },
    { image: '../../assets/food18.jpg', name: 'Astha Gill', show: false, id: 6 },
    { image: '../../assets/food13.jpg', name: 'Jaspreet', show: false, id: 7 },
    { image: '../../assets/food15.jpg', name: 'Guru Randhwa', show: false, id: 8 },
    { image: '../../assets/food16.jpg', name: 'Badshah', show: false, id: 9 },
  ];
  block: any;
  street: any;
  building: any;
  pickup: string;
  lat: any;
  lng: any;
  markers = [];
  constructor(
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public geolocation: Geolocation,
    public _ZONE: NgZone
  ) { }

  changeFilterPicup(lat: any, lng: any, ) {
    this.latitudeOrigin.next(lat);
    this.longitudeOrigin.next(lng);
  }
  completeAddr(CURRENT_ADDRESS: any) {
    this.ADDRESS_DETAIL.next(CURRENT_ADDRESS);
  }
  productsCount(TOTAL_COUNT: any) {
    this.totalCount.next(TOTAL_COUNT);
  }
  checkCouponStatus(COUPON_STATUS: boolean) {
    this.COUPON_COMING.next(COUPON_STATUS);
  }

  checkProfileData(USER_NAME: string, USER_ADDRESS: string, USER_CONTACT: number, USER_DESC: string, USER_IMAGE) {
    this.NAME_USER.next(USER_NAME);
    this.ADDRESS_USER.next(USER_ADDRESS);
    this.CONTACT_USER.next(USER_CONTACT);
    // this.LOC_USER.next(USER_LOC);
    this.DESC_USER.next(USER_DESC);
    this.IMAGE_USER.next(USER_IMAGE);
  }

  totalInputCount(INPUT_VALUE) {
    this.inputCount.next(INPUT_VALUE);
  }
  getLatLan(address: string) {
    const geocoder = new google.maps.Geocoder();
    // tslint:disable-next-line: max-line-length
    return new Observable((observer) => {
      geocoder.geocode({ address }, (results: { geometry: { location: any; }; }[], status: any) => {
        if (status === google.maps.GeocoderStatus.OK) {
          observer.next(results[0].geometry.location);
          observer.complete();
        } else {
          observer.next({ err: true });
          observer.complete();
        }
      });
    });
  }

  async getCurrentLoaction() {
    const loader = await this.loading('Getting your location...');
    loader.present();
    this.geolocation.getCurrentPosition().then((resp: { coords: { latitude: number; longitude: number; }; }) => {
      this._ZONE.run(() => {
        const latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
        const mapOptions = {
          center: latLng,
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.lat = resp.coords.latitude;
        this.lng = resp.coords.longitude;
        console.log('lat', this.lat, 'lng', this.lng);
        this.changeFilterPicup(this.lat, this.lng);
        this.getGeoLocation(resp.coords.latitude, resp.coords.longitude);
        loader.dismiss();
      });
      loader.dismiss();
    }).catch((error) => {
      console.log(error);
      loader.dismiss();
    }).finally(() => {
      loader.dismiss();
    });
    const watch = this.geolocation.watchPosition();
    watch.subscribe((data: any) => {
    });
  }
  async getGeoLocation(lat: number, lng: number) {
    if (navigator.geolocation) {
      const geocoder = await new google.maps.Geocoder();
      const latlng = await new google.maps.LatLng(lat, lng);
      const request = { latLng: latlng };

      await geocoder.geocode(request, (results: any[], status: any) => {
        if (status === google.maps.GeocoderStatus.OK) {
          const result = results[0];
          const rsltAdrComponent = result.address_components;
          if (result != null) {
            if (rsltAdrComponent[0] != null) {
              this.block = rsltAdrComponent[0].long_name;
              this.street = rsltAdrComponent[2].short_name;
              this.building = rsltAdrComponent[1].short_name;
              this.pickup = this.block + ' ' + this.street + ' ' + this.building;
              this.completeAddr(this.block);
            }
          } else {
            alert('No address available!');
          }
        }
      });
    }
  }

  async markerDragEnd($event: MouseEvent) {
    console.log('dragEnd', $event);
    if ($event.coords && $event.coords.lat && $event.coords.lng) {
      const geocoder = await new google.maps.Geocoder();
      const latlng = await new google.maps.LatLng($event.coords.lat, $event.coords.lng);
      const request = { latLng: latlng };

      await geocoder.geocode(request, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          const result = results[0];
          console.log(result);
          const rsltAdrComponent = result.address_components;
          console.log(rsltAdrComponent);
          if (result !== null) {
            if (rsltAdrComponent[0] !== null) {
              this.block = rsltAdrComponent[0].long_name;
              this.street = rsltAdrComponent[2].short_name;
              console.log(this.street);
              this.building = rsltAdrComponent[1].short_name;
              console.log(this.building);
            }
            this._ZONE.run(() => {
              this.lat = $event.coords.lat;
              this.lng = $event.coords.lng;
              this.changeFilterPicup(this.lat, this.lng);
              this.pickup = this.block + ' ' + this.street + ' ' + this.building;
            });

          } else {
            alert('No address available!');
          }

        }
      });
    }
  }
  mapClicked($event: MouseEvent) {
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
  }
  async opneModal(component?: any, props?: any, modalClass?: any) {
    const modal = await this.modalCtrl.create({
      component,
      componentProps: props,
      cssClass: modalClass,
      showBackdrop: true,
      backdropDismiss: true
    });
    return modal;
  }

  async loading(message: string) {
    const loader = await this.loadingCtrl.create({
      message
    });
    return loader;
  }

}
// tslint:disable-next-line: class-name
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
