/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-menumodal',
  templateUrl: './menumodal.component.html',
  styleUrls: ['./menumodal.component.scss'],
})
export class MenumodalComponent implements OnInit {
  menuList: { menu: string; id: string; }[];
  id: any;

  constructor(
    public serviceProvider: DeliveryService,
    public popCtrl: PopoverController
  ) {
    this.menuList = this.serviceProvider.menuList;
  }

  ngOnInit() { }

  getIdandClose(menuItems: any) {
    this.id = menuItems.id;
    this.popCtrl.dismiss({
      id: this.id
    });
    console.log(this.id);
  }
}
