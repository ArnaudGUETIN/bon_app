/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-myorders',
  templateUrl: './myorders.page.html',
  styleUrls: ['./myorders.page.scss'],
})
export class MyordersPage implements OnInit {
  previousOrders: { shopImg: string; shopName: string; totalItem: string; totalPrice: number; location: string; rating: string; }[];

  constructor(
    public serviceProvider: DeliveryService,
    public router: Router
  ) {
    this.previousOrders = this.serviceProvider.previousOrders;
  }

  ngOnInit() {
  }
  goForBilling(value) {
    console.log(value);
    const data = {
      totalPrice: value.totalItem * value.totalPrice,
      totalItem: value.totalItem,
      shopName: value.shopName,
      shopImg: value.shopImg
    };
    this.router.navigate(['billing', data]);
    this.serviceProvider.toggleCArt = true;
  }
}
