/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-restaurantoffers',
  templateUrl: './restaurantoffers.component.html',
  styleUrls: ['./restaurantoffers.component.scss'],
})
export class RestaurantoffersComponent implements OnInit {
  restroOffers: ({ head: string; subHead: string; slides: { text1: string; image: string; rating: string; time: string; price: string; }[]; } | { head: string; subHead: string; slides: { text1: string; text2: string; image: string; imgSmall: string; text: string; }[]; })[];
  itemDetails: ({ thumbnail: string; title: string; subTitle: string; imgSmall: string; text: string; rate: string; time: string; cost: string; famousFor?: undefined; slidesVertical?: undefined; slidesVerticalAvater?: undefined; } | {} | {} | {})[];

  constructor(
    public serviceProvider: DeliveryService,
    public route: Router
  ) {
    this.restroOffers = this.serviceProvider.offerSlides;
    this.itemDetails = this.serviceProvider.slidesHorizontal;

  }

  ngOnInit() { }
  goToProductPage(data: any) {
    this.route.navigate(['products', data]);
  }
  gotoRestuarentsProducts(items: any) {
    console.log(items);
    this.route.navigate(['restroproducts', items]);
  }
}
