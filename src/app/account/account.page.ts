/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';
import { EditprofileComponent } from '../editprofile/editprofile.component';
import { Router } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  USER_IMAGE = '../../assets/account.jfif';
  USER_NAME = 'Md Shadman Alam';
  USER_DESC = 'Full-Stack Developer';
  USER_ADDRESS = ' Kormangla, Bangalore';
  followers = '136 Followers';
  USER_CONTACT = 12345678;
  constructor(
    public serviceProvider: DeliveryService,
    public route: Router,
    public socialCtrl: SocialSharing
  ) { }

  ngOnInit() {
  }
  async  goToEditProfile() {
    this.serviceProvider.checkProfileData(this.USER_NAME, this.USER_ADDRESS, this.USER_CONTACT, this.USER_DESC, this.USER_IMAGE);
    const profileModal = await this.serviceProvider.opneModal(EditprofileComponent);
    await profileModal.present();
    await profileModal.onDidDismiss();
    this.serviceProvider.USER_NAME.subscribe(FILTER_USER_NAME => {
      this.USER_NAME = FILTER_USER_NAME;
      console.log(FILTER_USER_NAME);
    });
    this.serviceProvider.USER_ADDRESS.subscribe(FILTER_USER_ADDRESS => {
      this.USER_ADDRESS = FILTER_USER_ADDRESS;
      console.log(FILTER_USER_ADDRESS);
    });
    this.serviceProvider.USER_CONTACT.subscribe(FILTER_USER_CONTACT => {
      this.USER_CONTACT = FILTER_USER_CONTACT;
      console.log(FILTER_USER_CONTACT);
    });

    this.serviceProvider.USER_DESC.subscribe(FILTER_USER_DESC => {
      this.USER_DESC = FILTER_USER_DESC;
      console.log(FILTER_USER_DESC);
    });
    this.serviceProvider.USER_IMAGE.subscribe(FILTER_USER_IMAGE => {
      this.USER_IMAGE = FILTER_USER_IMAGE;
      console.log(FILTER_USER_IMAGE);
    });
  }
  goToMyOrders() {
    this.route.navigate(['myorders']);
  }
  goForHelp() {
    this.route.navigate(['help']);

  }
  goForChatHelp() {
    this.route.navigate(['chathelp']);

  }
  gotoShare() {
    // Check if sharing via email is supported
    this.socialCtrl.canShareViaEmail().then(() => {
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
    });

    // Share via email
    this.socialCtrl.shareViaEmail('Body', 'Subject', ['recipient@example.org']).then(() => {
      // Success!
    }).catch(() => {
      // Error!
    });
  }
}
