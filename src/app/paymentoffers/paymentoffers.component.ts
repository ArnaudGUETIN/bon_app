/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-paymentoffers',
  templateUrl: './paymentoffers.component.html',
  styleUrls: ['./paymentoffers.component.scss'],
})
export class PaymentoffersComponent implements OnInit {
  couponsList: { head: string; coupons: { coupImg: string; code: string; couTitle: string; coupDesc: string; }[]; }[];
  couponStatus: boolean;
  constructor(
    public serviceProvider: DeliveryService
  ) {
    this.couponsList = this.serviceProvider.paymentOffer;
    this.serviceProvider.COMING_COUPON.subscribe(status => {
      console.log(status);
      this.couponStatus = status;
    });
  }

  ngOnInit() { }
  goToapplyCoupon() {
    Swal.fire('Congrats !', 'Coupon Applied Successfully !', 'success');
  }
  goToapplyCopyCoupon() {
    if (!this.couponStatus) {
      Swal.fire('Congrats !', 'Coupon Copied Successfully !', 'success');
    }
  }
}
