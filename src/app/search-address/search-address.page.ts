import { Component, OnInit, NgZone } from '@angular/core';
import { DeliveryService } from '../delivery.service';
import { SetlocationComponent } from '../setlocation/setlocation.component';
import { NavController } from '@ionic/angular';
// import { google } from '@agm/core/services/google-maps-types';
declare var google;
@Component({
  selector: 'app-search-address',
  templateUrl: './search-address.page.html',
  styleUrls: ['./search-address.page.scss'],
})
export class SearchAddressPage implements OnInit {
  locationInput = '';
  autocompleteItems: any[];
  lat: any;
  lng: any;
  constructor(
    public serviceProvider: DeliveryService,
    public __ZONE: NgZone,
    public navCtrl: NavController) { }

  ngOnInit() {
  }
  async goToCHeckOnMap() {
    const modal = await this.serviceProvider.opneModal(SetlocationComponent);
    await modal.present();
  }
  searcOnChnage() {
    if (this.locationInput) {
      const service = new google.maps.places.AutocompleteService();
      // tslint:disable-next-line: max-line-length
      service.getPlacePredictions({ input: this.locationInput, componentRestrictions: { country: this.serviceProvider.locatedCountry ? this.serviceProvider.locatedCountry : 'IN' } }, (predictions, status) => {
        this.autocompleteItems = [];
        this.__ZONE.run(() => {
          if (predictions != null) {
            predictions.forEach((prediction) => {
              this.autocompleteItems.push(prediction.description);
            });
          }
        });
      });
    }
  }

  chooseItem(e: any) {
    this.serviceProvider.completeAddr(e);
    this.serviceProvider.getLatLan(e).subscribe((result: any) => {
      console.log(result);
      this.__ZONE.run(() => {
        this.lat = result.lat();
        this.lng = result.lng();
        this.serviceProvider.changeFilterPicup(this.lat, this.lng);
        this.openModal();
      });
    }, error => console.log(error),
      () => console.log('pickup completed'));
  }
  async openModal() {
    const modal = await this.serviceProvider.opneModal(SetlocationComponent);
    await modal.present();
  }
  getCurrentLocation() {
    this.serviceProvider.getCurrentLoaction();
    this.openModal();
  }
  closePage() {
    this.navCtrl.back();
  }
}
