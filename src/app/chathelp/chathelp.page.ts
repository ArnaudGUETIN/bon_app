
/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { IonContent } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { ChatPopoverComponent } from '../chat-popover/chat-popover.component';
@Component({
  selector: 'app-chathelp',
  templateUrl: './chathelp.page.html',
  styleUrls: ['./chathelp.page.scss'],
})
export class ChathelpPage implements OnInit {

  @ViewChild('IonContent', { static: true }) content: IonContent;

  data: {}[];
  customPopoverOptions: any = {
    header: 'Chat Service',
    subHeader: 'Select your chat query',
    message: 'Only select your dominant chat query'
  };
  paramData: any;
  msgList: any;
  userName: any;
  USER_INPUT = '';
  User = 'Me';
  toUser = 'HealthBot';
  START_TYPING: any;
  loader: boolean;
  show: boolean;
  footerJson: { 'icon': string; 'label': string; }[];

  constructor(public activRoute: ActivatedRoute, public popCtrl: PopoverController) {
    this.data = [{
      text: 'Thursday 31 January 2019',
    }];
    this.activRoute.params.subscribe((params) => {
      console.log(params);
      this.paramData = params;
      this.userName = params.name;
    });
    this.msgList = [
      {
        userId: 'HealthBot',
        userName: 'HealthBot',
        userAvatar: '../../assets/food19.jpg',
        time: '12:00',
        message: 'Hello, my order is late for more than 30 mins',
        id: 0,
        status: 'checkmark'
      },
      {
        userId: 'Me',
        userName: 'Me',
        userAvatar: '../../assets/food15.jpg',
        time: '12:03',
        message: 'Hi, let me track down your order, please give me 1 min ',
        id: 1,
        status: 'checkmark',
        name: 'Diana Nicole'

      },
      {
        userId: 'HealthBot',
        userName: 'HealthBot',
        userAvatar: '../../assets/food19.jpg',
        time: '12:05',
        message: 'Ok sure',
        id: 3,
        status: 'done-all'
      },
      {
        userId: 'Me',
        userName: 'Me',
        userAvatar: '../../assets/food15.jpg',
        time: '12:06',
        message: 'It looks like there is heavy rain in the area, the delivery might take 10-15 mins more. We apologize for the incovenience. Would you like to wait or cancel the order?',
        id: 4,
        status: 'checkmark',
        name: 'Diana Nicole'

      },
      {
        userId: 'HealthBot',
        userName: 'HealthBot',
        userAvatar: '../../assets/food19.jpg',
        time: '12:07',
        message: 'Ok, no problem. I can wait, thanks.',
        id: 5,
        status: 'done-all'
      }
    ];
  }

  ngOnInit() {
  }
  typeSelected(type: any) {
    console.log(type);
    if (this.USER_INPUT === '' && type.icon === 'images') {
      this.msgList.push({
        userId: this.toUser,
        userName: this.toUser,
        time: '12:01',
        image: '../../assets/food17.jpg',
        id: this.msgList.length + 1,
        status: 'checkmark'
      });
      this.USER_INPUT = '';
      this.show = false;
      this.scrollDown();
      setTimeout(() => {
        this.senderSends();
      }, 500);
    }
  }

  sendMsg() {
    if (this.USER_INPUT !== '') {
      this.msgList.push({
        userId: this.toUser,
        userName: this.toUser,
        userAvatar: this.paramData.image ? this.paramData.image : '../../assets/food5.jpg',
        time: '12:01',
        message: this.USER_INPUT,
        id: this.msgList.length + 1,
        status: 'checkmark'

      });
      this.USER_INPUT = '';
      this.scrollDown();
      setTimeout(() => {
        this.senderSends();
      }, 500);
    }
    this.show = false;
  }
  senderSends() {
    this.loader = true;
    setTimeout(() => {
      this.msgList.push({
        userId: this.User,
        userName: this.User,
        userAvatar: '../../assets/food15.jpg',
        time: '12:01',
        message: 'Sorry, didn\'t get what you said. Can you repeat that please',
        status: 'checkmark',
        name: 'Diana Nicole'

      });
      this.loader = false;
      this.scrollDown();
    }, 2000);
    this.scrollDown();
  }
  scrollDown() {
    setTimeout(() => {
      this.content.scrollToBottom(50);
    }, 200);
  }
  something($event: any) {
    $event.preventDefault();
    console.log($event);
  }
  userTyping(event: any) {
    this.show = false;
    console.log(event);
    this.START_TYPING = event.target.value;
    this.scrollDown();
  }
  focusFunction(event: any) {
    this.show = false;
    console.log(event);
  }
  async popoverOpen(ev: any) {
    console.log(ev);
    const popover = await this.popCtrl.create({
      component: ChatPopoverComponent,
      translucent: true,
      event: ev,
      mode: 'md'
    });
    return await popover.present();

  }
}



