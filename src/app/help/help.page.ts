/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { DeliveryService } from '../delivery.service';
import { HelpdetailComponent } from '../helpdetail/helpdetail.component';

@Component({
  selector: 'app-help',
  templateUrl: './help.page.html',
  styleUrls: ['./help.page.scss'],
})
export class HelpPage implements OnInit {

  public questions: any;
  public faqs;

  constructor(
    public dataServ: DeliveryService,
    private modalController: ModalController,
    private route: Router,
  ) {

    this.faqs = dataServ.needHelp;
    this.questions = Object.keys(dataServ.needHelp);
    console.log('this.questions', this.questions);
  }

  ngOnInit() {
  }

  async openIssue(i, question) {
    const modal = await this.modalController.create({
      component: HelpdetailComponent,
      componentProps: { value: Object.values(this.faqs)[i], title: question }
    });
    return await modal.present();
  }

  // async searchPage() {
  //   // this.route.navigate(['search'])
  //   let modal = await this.modalController.create({
  //     component: SearchPage,
  //   });
  //   return await modal.present();
  // }
  // cartPage() {
  //   this.route.navigate(['cart'])
  // }

}
