/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.scss'],
})
export class EditprofileComponent implements OnInit {
  USER_NAME: string;
  USER_ADDRESS: string;
  USER_CONTACT: number;
  USER_DESC: string;
  USER_IMAGE: string;
  name: string;
  address: string;
  contact: number;
  image: string;
  desc: string;
  password = '';
  email = 'store.enappd@enappd.com';
  constructor(
    public serviceProvider: DeliveryService
  ) {
    this.serviceProvider.USER_NAME.subscribe(FILTER_USER_NAME => {
      this.USER_NAME = FILTER_USER_NAME;
      console.log(FILTER_USER_NAME);
    });
    this.serviceProvider.USER_ADDRESS.subscribe(FILTER_USER_ADDRESS => {
      this.USER_ADDRESS = FILTER_USER_ADDRESS;
      console.log(FILTER_USER_ADDRESS);
    });
    this.serviceProvider.USER_CONTACT.subscribe(FILTER_USER_CONTACT => {
      this.USER_CONTACT = FILTER_USER_CONTACT;
      console.log(FILTER_USER_CONTACT);
    });

    this.serviceProvider.USER_DESC.subscribe(FILTER_USER_DESC => {
      this.USER_DESC = FILTER_USER_DESC;
      console.log(FILTER_USER_DESC);
    });
    this.serviceProvider.USER_IMAGE.subscribe(FILTER_USER_IMAGE => {
      this.USER_IMAGE = FILTER_USER_IMAGE;
      console.log(FILTER_USER_IMAGE);
    });
    // this.profileData = [
    this.name = this.USER_NAME;
    this.address = this.USER_ADDRESS;
    this.contact = this.USER_CONTACT;
    this.desc = this.USER_DESC;
    this.image = this.USER_IMAGE;
    // ]
  }

  ngOnInit() { }
  closePage() {
    this.serviceProvider.modalCtrl.dismiss();
  }
  goToUPdateData() {
    this.serviceProvider.checkProfileData(this.name, this.address, this.contact, this.desc, this.image);
    this.serviceProvider.modalCtrl.dismiss();

  }
}
