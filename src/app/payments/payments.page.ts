/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.page.html',
  styleUrls: ['./payments.page.scss'],
})
export class PaymentsPage implements OnInit {
  paymentMethods: ({ head: string; items: { thumbRight: string; textLeft: string; text: string; }[]; } | { head: string; items: ({ thumbRight: string; text: string; icon?: undefined; icontext?: undefined; iconpara?: undefined; } | {})[]; } | {})[];

  constructor(
    public serviceProvider: DeliveryService,
    public route: Router) {
    this.paymentMethods = this.serviceProvider.payments;
  }

  ngOnInit() {
  }
  goToPayCharge(items: any) {
    console.log(items.id);
    switch (items.id) {
      case 'cash':
        Swal.fire(
          {
            title: 'Are you sure?',
            text: 'Once ordered, you will not be able to cancel your order !',
            type: 'warning',
            // dangerMode: true,
          }
        )
          .then((willDelete) => {
            if (willDelete) {
              Swal.fire('Yay', 'Congrat\'s ! Order placed successfully !',
                'success',
              );
              this.route.navigate(['/tabs/hunger']);
              this.serviceProvider.toggleCArt = false;
            } else {
              Swal.fire('Oops ! You have cancelled your order !');
            }
          });
        break;
      case 'google':
        items.clicked = !items.clicked;
        break;
      case 'visa':
        items.clicked = !items.clicked;
        break;
    }
  }
}
