/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
  segmentValue: any;
  radios: { name: string; }[];
  radioItemNo = 'Relevance';
  checkboxes: { name: string; value: string; }[];
  offers: { name: string; value: string; }[];
  constructor(
    public serviceProvider: DeliveryService
  ) {
    this.radios = this.serviceProvider.radios;
    this.checkboxes = this.serviceProvider.checkboxes;
    this.offers = this.serviceProvider.offers;
  }

  ngOnInit() { }
  segmentChanged(events) {
    this.segmentValue = events.detail.value;
  }
  closePage() {
    this.serviceProvider.modalCtrl.dismiss();
  }
}
