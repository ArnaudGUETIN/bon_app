/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeliveryService } from '../delivery.service';
import { AddaddressComponent } from '../addaddress/addaddress.component';
import { AddressListComponent } from '../address-list/address-list.component';

@Component({
  selector: 'app-billing',
  templateUrl: './billing.page.html',
  styleUrls: ['./billing.page.scss'],
})
export class BillingPage implements OnInit {
  BILLING_DATA: any;
  totalAmount: number;
  totalItemCount: number;
  noteTips = 'Any request for the restaurant? write here..';
  Location: any;
  address: any;
  time: any;
  cartImage = '../../assets/cart.png';
  constructor(
    public actvRoute: ActivatedRoute,
    public route: Router,
    public serviceProvider: DeliveryService
  ) {
    this.actvRoute.params.subscribe(paramValues => {
      this.BILLING_DATA = paramValues;
      this.totalItemCount = parseInt(this.BILLING_DATA.totalItem, 10);
      this.totalAmount = parseInt(this.BILLING_DATA.totalPrice, 10);
      console.log(this.BILLING_DATA);
    });
    this.getCurrentLocation();
  }

  ngOnInit() {
  }
  counterButton(value: string) {
    if (value === 'add') {
      this.totalItemCount += 1;
      this.totalAmount = this.totalAmount + 123;
    } else if (value === 'remove') {
      this.totalItemCount -= 1;
      this.totalAmount = this.totalAmount - 123;

    }
  }
  getCurrentLocation() {
    this.serviceProvider.COMPLETE_ADDRESS.subscribe(filter => {
      this.Location = filter;
      console.log(this.Location);
    });
  }
  async goToAddAddress(value: string) {
    if (value === 'add') {
      const modal = await this.serviceProvider.opneModal(AddaddressComponent);
      await modal.present();
      await modal.onDidDismiss();
      this.getCurrentLocation();
      this.address = 'Albert Street, New York';
      this.time = '23 Mins';
    } else if (value === 'select') {
      const modal = await this.serviceProvider.opneModal(AddressListComponent);
      await modal.present();
      const data = await modal.onDidDismiss();
      this.address = data.data.data.name;
      this.time = data.data.data.time;
      console.log(this.address, data);
    }
  }
  goToPayments() {
    this.route.navigate(['payments']);
  }
  goTOBrowseRestaurants() {
    this.route.navigate(['/tabs/hunger']);
  }
  goTOApplyCoupon() {
    this.route.navigate(['offers']);
    this.serviceProvider.checkCouponStatus(true);
  }
  // tslint:disable-next-line: use-life-cycle-interface
  ngOnDestroy() {
    this.serviceProvider.toggleCArt = false;
  }
}
