import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeliveryService } from '../delivery.service';
import { MenumodalComponent } from '../menumodal/menumodal.component';
import { PopoverController, IonContent } from '@ionic/angular';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-restroproducts',
  templateUrl: './restroproducts.page.html',
  styleUrls: ['./restroproducts.page.scss'],
})
export class RestroproductsPage implements OnInit {
  @ViewChild(IonContent, { static: true }) content: IonContent;

  restroData: any;
  // clicked: boolean;
  public inputValue = 1;
  segmentButton: any;
  prevOrders: any;
  scrollTiId: any;
  totalItemsQuantity = [];
  totalQuantity: any;
  totalCost: number;
  restroName: string;
  restroImage: string;
  Items = 0;
  constructor(
    public actvRoute: ActivatedRoute,
    public serviceProvider: DeliveryService,
    public popCtrl: PopoverController,
    public route: Router
  ) {
    this.actvRoute.params.subscribe(filterRestro => {
      this.restroData = filterRestro;
      this.restroName = this.restroData.title;
      this.restroImage = this.restroData.thumbnail;
      console.log(this.restroName);
    });
    this.prevOrders = this.serviceProvider.restorentProducts;
    console.log(this.prevOrders);
  }

  ngOnInit() {
  }

  toggleAddValue(value: { clicked: boolean; }, index: any) {
    console.log(value, index);
    value.clicked = true;
  }

  counterButton(card: { inputValue: number; clicked: boolean; dishPrice: number, price: number, cost: number }, value: string, index: any) {
    if (value === 'add') {
      this.serviceProvider.totalInputCount(card.inputValue);
      card.inputValue += 1;
      this.totalItemsQuantity.push(card.inputValue);
      this.totalQuantity = this.totalItemsQuantity.length;
      this.Items = this.totalQuantity;
      this.totalCost = (this.totalQuantity) * card.cost || (this.totalQuantity) * card.dishPrice || (this.totalQuantity) * card.price;
    } else if (value === 'remove') {
      card.inputValue -= 1;
      this.totalItemsQuantity.pop();
      this.totalQuantity = this.totalItemsQuantity.length;
      this.Items = this.totalQuantity;
      this.totalCost = (this.totalCost) - card.cost || (this.totalCost) - card.dishPrice || (this.totalCost) - card.price;
    }
    if (card.inputValue <= 0) {
      card.clicked = false;
      console.log(card.clicked);
    }
  }

  openAccordian = (data: any, ind: any) => {
    console.log(data, ind);
    data.acordian = !data.acordian;
  }

  async openMenuModal(ev: any) {
    const popover = await this.popCtrl.create({
      component: MenumodalComponent,
      event: ev,
      translucent: true
    });
    await popover.present();
    const data = await popover.onDidDismiss();
    this.scrollTiId = data.data.id;
    console.log(this.scrollTiId);
    this.scrollTo();

  }
  scrollTo() {
    this.content.scrollByPoint(0, this.scrollTiId, 10);
  }
  gotoCart() {
    const data = {
      totalPrice: this.totalCost,
      totalItem: this.totalQuantity,
      shopName: this.restroName,
      shopImg: this.restroImage
    };
    if (this.totalQuantity <= 0 || this.Items <= 0) {
      Swal.fire('Wait !', 'Please add some item first !', 'error');
    } else {
      this.route.navigate(['billing', data]);
      this.serviceProvider.toggleCArt = true;
    }

  }

}
