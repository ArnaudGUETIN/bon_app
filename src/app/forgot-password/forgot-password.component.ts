/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DeliveryService } from '../delivery.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {
  title = 'Recover Your Account';
  emailInput = 'Email address or username';
  description = 'Enter your Foodie username, or the email address that you used to register.we\'ll send send you an email to recover your account password.';
  recover = '';
  help = 'Still need help? Click here!';
  loginPic = '../../assets/login.jpg';

  constructor(
    public route: Router,
    public serviceProvider: DeliveryService) { }

  ngOnInit() { }
  goToRecovery() {
    Swal.fire('Congrats !', 'Go and check your mail !', 'success');
    this.route.navigate(['login']);
    this.serviceProvider.modalCtrl.dismiss();
  }
  goTocreateAccount() {
    Swal.fire('Congrats !', 'Go and check your mail !', 'success');

    this.route.navigate(['login']);
    this.serviceProvider.modalCtrl.dismiss();
  }
  closeModal() {
    this.serviceProvider.modalCtrl.dismiss();

  }
}
