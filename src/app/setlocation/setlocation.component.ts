/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';
import { MouseEvent } from '@agm/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-setlocation',
  templateUrl: './setlocation.component.html',
  styleUrls: ['./setlocation.component.scss'],
})
export class SetlocationComponent implements OnInit {
  pickupLat: any;
  pickupLng: any;
  markers = [];
  zoom = 8;
  public markerOptions = {
    origin: {
      animation: '\'DROP\'',
      label: 'origin',
    }
  };
  public renderOptions = {
    suppressMarkers: true,
  };
  Location: any;
  constructor(
    public serviceProvider: DeliveryService,
    public route: Router) {
    this.serviceProvider.originlatitude.subscribe(filter => {
      this.pickupLat = filter;
    });
    this.serviceProvider.originlongititude.subscribe(filter => {
      this.pickupLng = filter;
    });
    this.serviceProvider.COMPLETE_ADDRESS.subscribe(filter => {
      this.Location = filter;
      console.log(this.Location);
    });
  }

  ngOnInit() { }
  mapClicked($event: MouseEvent) {
    this.serviceProvider.mapClicked($event);
  }
  getCurrentLocation() {
    this.serviceProvider.getCurrentLoaction();
  }
  markerDragEnd($event: MouseEvent) {
    this.serviceProvider.markerDragEnd($event);
  }
  locationConfirmed() {
    this.route.navigate(['/tabs/hunger']);
    this.serviceProvider.modalCtrl.dismiss();
  }
}
// tslint:disable-next-line: class-name
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
