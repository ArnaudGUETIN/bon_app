/**
 * Ionic 4 Food delivery/search app starter (https://store.enappd.com/product/ionic-4-food-delivery-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AgmCoreModule } from '@agm/core';
import { SetlocationComponent } from './setlocation/setlocation.component';
import { FilterComponent } from './filter/filter.component';
import { MenumodalComponent } from './menumodal/menumodal.component';
import { AddaddressComponent } from './addaddress/addaddress.component';
import { FormsModule } from '@angular/forms';
import { AddressListComponent } from './address-list/address-list.component';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { HelpdetailComponent } from './helpdetail/helpdetail.component';
import { SearchmodalComponent } from './searchmodal/searchmodal.component';
import { ChatPopoverComponent } from './chat-popover/chat-popover.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@NgModule({
  declarations: [
    AppComponent,
    SetlocationComponent,
    FilterComponent,
    MenumodalComponent,
    AddaddressComponent,
    AddressListComponent,
    EditprofileComponent,
    HelpdetailComponent,
    SearchmodalComponent,
    ChatPopoverComponent,
    ForgotPasswordComponent
  ],
  entryComponents: [
    SetlocationComponent,
    FilterComponent,
    MenumodalComponent,
    AddaddressComponent,
    AddressListComponent,
    EditprofileComponent,
    HelpdetailComponent,
    SearchmodalComponent,
    ChatPopoverComponent,
    ForgotPasswordComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAP_Xy-1QSclKYAvxSmAZO2BuFAWWAlOZQ',
      libraries: ['places']
    })
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    SocialSharing,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
