/**
 * Ionic 4 Food Ordering app starter (https://store.enappd.com/product/ionic-4-food-ordering-app-starter)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'signup',
    pathMatch: 'full'
  },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule) },
  { path: 'tabs', loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule) },
  { path: 'account', loadChildren: () => import('./account/account.module').then(m => m.AccountPageModule) },
  { path: 'search-address', loadChildren: () => import('./search-address/search-address.module').then(m => m.SearchAddressPageModule) },
  { path: 'products', loadChildren: () => import('./products/products.module').then(m => m.ProductsPageModule) },
  { path: 'restroproducts', loadChildren: () => import('./restroproducts/restroproducts.module').then(m => m.RestroproductsPageModule) },
  { path: 'billing', loadChildren: () => import('./billing/billing.module').then(m => m.BillingPageModule) },
  { path: 'payments', loadChildren: () => import('./payments/payments.module').then(m => m.PaymentsPageModule) },
  { path: 'offers', loadChildren: () => import('./offers/offers.module').then(m => m.OffersPageModule) },
  { path: 'myorders', loadChildren: () => import('./myorders/myorders.module').then(m => m.MyordersPageModule) },
  { path: 'help', loadChildren: () => import('./help/help.module').then(m => m.HelpPageModule) },
  { path: 'chathelp', loadChildren: () => import('./chathelp/chathelp.module').then(m => m.ChathelpPageModule) },
  { path: 'signup', loadChildren: () => import('./signup/signup.module').then(m => m.SignupPageModule) },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
